import { showModal } from './modal.mjs';
import { createElement } from '../helpers/domHelper.mjs';

export function showWinnerModal(winners, onClose) {
  const winnerDescription = createElement({
    tagName: 'div',
    className: 'modal-body',
  });

  const winnerList = createElement({
    tagName: 'ol'
  });

  winners.forEach(({ name, timeSpent, isTextCompleted, percentageProgress }) => {
    const listItem = createElement({
      tagName: 'li',
      className: 'winners-list-item'
    });

    listItem.innerText = isTextCompleted
      ? `${name}: spent ${timeSpent} seconds`
      : `${name}: doesn't write all text (${percentageProgress}%)`;

    winnerList.append(listItem);
  });

  winnerDescription.append(winnerList);

  showModal({
    title: `List of winners: `,
    bodyElement: winnerDescription,
    onClose
  })
}