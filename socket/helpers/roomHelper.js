import * as config from '../config';
import { availableRooms } from '../index'

export const getRoomEntity = (io, roomName) => io.sockets.adapter.rooms[roomName];
export const roomExists = (io, roomName) => getRoomEntity(io, roomName) ? true : false;

export function toggleRoomAvailabilityIfNeeds(io, roomName) {
  const room = getRoomEntity(io, roomName);

  if (!room || room.length === config.MAXIMUM_USERS_FOR_ONE_ROOM) {
    delete availableRooms[roomName];
    return;
  }

  if (room.length <= config.MAXIMUM_USERS_FOR_ONE_ROOM - 1) {
    availableRooms[roomName] = true;
  }
}

export const getAvailableRooms = (io) => {
  return Object.keys(availableRooms)
    .map((key) => ({
      name: key,
      userCount: getRoomEntity(io, key).length
    }));
};

export const getRoomDetails = (io, roomName) => ({
  name: roomName,
  users: Object.keys(io.sockets?.adapter.rooms[roomName].sockets)
    .map(socketId => io.sockets?.connected[socketId].userData),
});

export const sendRoomsList = (io, socket) => socket.emit('ROOMS_LIST', getAvailableRooms(io));
export const updateRoom = (io, roomName) => io.in(roomName).emit('UPDATE_ROOM', getRoomDetails(io, roomName));

export const clearRoom = async (io, roomName) => {
  if (!roomExists(io, roomName)) {
    return;
  }
  const users = getRoomDetails(io, roomName).users;
  users.forEach(user => {
    user.ready = false;
    user.progress = 0;
    user.typed = 0;
  });

  await io.in(roomName).emit('UPDATE_ROOM', getRoomDetails(io, roomName));
};

export function joinSocketToRoom(io, socket, roomName, callback) {
  socket.userData = {
    ...socket.userData,
    ready: false,
    progress: 0,
    lastPress: null,
    typed: 0
  };
  socket.roomName = roomName;

  socket.join(roomName, () => {
    sendRoomsList(io, socket.broadcast);
    updateRoom(io, roomName);

    callback();
  });
}