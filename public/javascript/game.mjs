import { createElement } from '../javascript/helpers/domHelper.mjs';
import { showModal } from '../javascript/modal/modal.mjs';
import { showWinnerModal } from "../javascript/modal/winner.mjs";

const username = sessionStorage.getItem('username');

const moveToLoginPage = () => window.location.replace('/login');

if (!username) moveToLoginPage();

const socket = io('', { query: { username } });

socket.on('error', (err) => {
  showModal({
    title: 'Error!',
    bodyElement: err,
    onClose: () => moveToLoginPage()
  });
  sessionStorage.clear();

});

const root = document.getElementById('root');
const roomsPage = document.getElementById('rooms-page');
const gamePage = document.getElementById('game-page');

socket.on('connect', () => {
  const waitingMsg = document.getElementById('waiting-msg');
  waitingMsg.classList.add('display-none');
  roomsPage.classList.remove('display-none');
});

const roomTitle = document.getElementById('room-title');

const updateRoomInterface = ({ name, users }) => {
  roomTitle.innerHTML = name;

  playersList.innerHTML = '';
  users.forEach(user => {
    playersList.append(createPlayerInfo(user));

    if (user.name === username) {
      readyButton.innerHTML = user.ready ? 'Not Ready' : 'Ready';
    }
  });
};

socket.on('UPDATE_ROOM', updateRoomInterface);

const backToRooms = () => {
  socket.emit('LEAVE_ROOM');
  roomsPage.classList.remove('display-none');
  gamePage.classList.add('display-none');
}

const backToRoomsButton = document.getElementById('back-to-rooms-button');
backToRoomsButton.addEventListener('click', backToRooms);

const readyButton = document.getElementById('ready-button');
readyButton.addEventListener('click', () => {
  socket.emit('TOGGLE_READY');
});

const playersList = document.getElementById('players-list');

function updatePlayerProgress(username, progress) {
  const progressEl = document.getElementById(`${username}-player-progress`);
  progressEl.style.width = progress * 100 + '%';

  if(progress === 1) {
    progressEl.className += " full";
  }
}

const createPlayerInfo = (user) => {
  const playerInfo = createElement({ tagName: 'div', className: 'player-info' });

  if (user.ready) {
    playerInfo.className += ' ready';
  }

  if (user.name === username) {
    playerInfo.className += ' you';
  }

  const playerName = createElement({ tagName: 'p', className: 'player-name' });
  playerName.innerHTML = user.name;

  const playerProgressWrapper = createElement({ tagName: 'div', className: 'player-progress-wrapper' });

  const playerProgressEl = createElement({
    tagName: 'div',
    className: 'player-progress', 
    attributes: { id: `${user.name}-player-progress` }
  });

  playerProgressEl.style.width = '0px';

  playerProgressWrapper.append(playerProgressEl);
  playerInfo.append(playerName, playerProgressWrapper);

  return playerInfo;
};

const joinRoom = roomName => {
  socket.emit('JOIN_ROOM', { roomName }, error => {
    if (error) {
      showModal({ title: 'error', bodyElement: error });
      return;
    }

    roomsPage.classList.add('display-none');
    gamePage.classList.remove('display-none');
  });
};

const roomsList = document.getElementById('rooms-list');
const createNewRoom = (room) => {
  const roomDiv = createElement({ tagName: 'div', className: 'room-desc' });

  const usersNumber = createElement({ tagName: 'p', className: 'users-number' });
  usersNumber.innerHTML = `${room.userCount} user connected`;

  const roomName = createElement({ tagName: 'p', className: 'room-name logical-block flex-centered' });
  roomName.innerHTML = room.name;

  const buttonWrapper = createElement({ tagName: 'div', className: 'flex-centered full-width logical-block' });

  const joinButton = createElement({ tagName: 'button', className: 'medium-button flex-centered' })
  joinButton.innerHTML = 'Join';
  joinButton.addEventListener('click', () => joinRoom(room.name));

  buttonWrapper.append(joinButton);
  roomDiv.append(usersNumber, roomName, buttonWrapper);
  roomsList.appendChild(roomDiv);
};

const createRoomButton = document.getElementById('create-room-button');

const onClickCreateRoomButton = () => {
  const newName = document.getElementById('create-room-input').value;
  if (!newName) return; 

  socket.emit('NEW_ROOM', newName , (error) => {
    if (!error) {
      roomsPage.classList.add('display-none');
      gamePage.classList.remove('display-none');
    } else {
      showModal({ title: 'error', bodyElement: error });
    }
  });
}
createRoomButton.addEventListener('click', onClickCreateRoomButton);

socket.on('ROOMS_LIST', (rooms) => {
  roomsList.innerHTML = '';
  rooms.forEach(room => {
    createNewRoom(room);
  })
})

const countdownEL = document.getElementById('countdown');

const updateCountdown = (countdown) => {
  countdownEL.innerHTML = countdown;
}

const textContainer = document.getElementById('text-container');
const gameTimerLabel = document.getElementById('game-timer');
const typedLettersElem = document.getElementById('typed-letters');
const nextLetterElem = document.getElementById('next-letter');
const remainingLetters = document.getElementById('remaining-letters');

const updateUserTypedText = ({ typedText, nextLetter, textLeft }) => {
  typedLettersElem.innerHTML = typedText;
  nextLetterElem.innerHTML = nextLetter;
  remainingLetters.innerHTML = textLeft;
};

const updateGameTimer = (secsToGameEnd) => {
  gameTimerLabel.innerHTML = `${secsToGameEnd} seconds left`;
};

function keyBoardEventListener({ key }) {
  socket.emit('USER_TYPING', key);
};

const startGame = (text) => {
  countdown.classList.add('display-none');
  textContainer.classList.remove('display-none');
  gameTimerLabel.classList.remove('display-none');
  typedLettersElem.innerText = '';
  nextLetterElem.innerText = text[0];
  remainingLetters.innerText = text.substring(1);

  document.addEventListener('keydown', keyBoardEventListener);
};

const endGame = (winnersList) => {
  document.removeEventListener('keydown', keyBoardEventListener);

  textContainer.classList.add('display-none');
  gameTimerLabel.classList.add('display-none');

  showWinnerModal(winnersList, () => {
    backToRoomsButton.classList.remove('display-none');
    readyButton.classList.remove('display-none');
  });
}

const prepareTimerStartInterface = ({ countdown, secsToGameEnd }) => {
  backToRoomsButton.classList.add('display-none');
  readyButton.classList.add('display-none');

  countdownEL.classList.remove('display-none');
  updateCountdown(countdown);
  updateGameTimer(secsToGameEnd);
}

socket.on('GAME_SCHEDULED', prepareTimerStartInterface)

socket.on('START_GAME', startGame)

socket.on('UPDATE_COUNTDOWN', (countdown) => {
  updateCountdown(countdown);
});

socket.on('UPDATE_GAME_TIMER', (secsToGameEnd) => {
  updateGameTimer(secsToGameEnd);
});

socket.on('UPDATE_PROGRESS', updatePlayerProgress);

socket.on('UPDATE_USER_TYPED_TEXT', updateUserTypedText);

socket.on('END_GAME', (winnersList) => {
  endGame(winnersList);
}); 